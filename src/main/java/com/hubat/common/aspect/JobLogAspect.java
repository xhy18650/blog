package com.hubat.common.aspect;

import com.hubat.common.annotation.Log;
import com.hubat.common.domain.LogDO;
import com.hubat.common.service.LogService;
import com.hubat.common.utils.HttpContextUtils;
import com.hubat.common.utils.IPUtils;
import com.hubat.common.utils.JSONUtils;
import com.hubat.common.utils.ShiroUtils;
import com.hubat.system.domain.UserDO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 *  定时切面 日记
 */
@Aspect
@Component
public class JobLogAspect {
    private static final Logger logger = LoggerFactory.getLogger(JobLogAspect.class);

    @Autowired
    LogService logService;


    @Pointcut("@annotation(com.hubat.common.annotation.JobLog)")
    public void JoblogPointCut() {
    }

    @Around("JoblogPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime = System.currentTimeMillis();
        // 执行方法
        Object result = point.proceed();
        // 执行时长(毫秒)
        long times = System.currentTimeMillis() - beginTime;
        //异步保存日志
        saveJobLog(point, times);
        return result;
    }

    void saveJobLog(ProceedingJoinPoint joinPoint, long times) throws InterruptedException {

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Class classz =  signature.getClass();
        String targetName = signature.getName();//类名
        String methodName = signature.getName();//方法名
        Object[] objects =  joinPoint.getArgs();
        JobExecutionContext context = (JobExecutionContext )objects[0];
      //  Object o = context.getMergedJobDataMap().get();

    }
}
