package com.hubat.common.task;

import com.hubat.common.annotation.JobLog;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class WelcomeJob implements Job{

    public static final Logger logger = LoggerFactory.getLogger(WelcomeJob.class);


    @JobLog
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
    	//template.convertAndSend("/topic/getResponse", new Response("欢迎体验hubat,这是一个任务计划，使用了websocket和quzrtz技术，可以在计划列表中取消，欢迎您加入qq群交流学习!" ));
        logger.info("欢迎体验hubat,这是一个任务计划");
    }

}